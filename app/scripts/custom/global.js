var initGlobal = function() {

	/* 
		Accordion
	*/

	$('[data-js="accordion"]').each(function(){
		var $this = $(this),
			$item = $('[data-js="accordionItem"]'),
			$trigger = $item.find('[data-js="accordionTrigger"]'),
			$content = $item.find('[data-js="accordionContent"]'),
			events = $('html').hasClass('desktop') ? 'click' : 'click';


		/* If Desktop/no-touch, toggle active class on hover
		   Else, trigger on click/touch */
		
		$trigger.on(events, function(){
			var $parent = $(this).parents('[data-js="accordionItem"]'),
				target = $(this);

			// Display content
			$item.not($parent).removeClass('is-active');
			$parent.toggleClass('is-active');

		});
	});
}
